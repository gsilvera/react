// Import the React and ReactDom libraries
import React from 'react';
import ReactDOM from 'react-dom';

// Create a react component
// Component: es una clase o función que produce html para mostrar al usuario (jsx) y maneja las respuesta de este (event handlers)
// => es igual a function
const App = () => {
    return <div>Hola mundo!</div>;
};


// Take the react component and show it on screen
ReactDOM.render(
    <App />,
    document.querySelector('#root')
    );